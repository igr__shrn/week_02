package ru.edu;

import org.junit.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileStatisticReporterTest {

    public static final String RESULT_TXT = "./target/reporterResultTest.txt";

    @Test
    public void writeStats() {
        StatisticReporter reporter = new FileStatisticReporter(RESULT_TXT);

        TextStatistics statistics = new TextStatistics(10);

        statistics.addCharsCount(500);
        statistics.addWordsCount(100);
        statistics.addCharsCountOnlyPunctuations(25);
        statistics.addCharsCountWithoutSpaces(20);
        statistics.addWordsMap("test");

        reporter.report(statistics);

        List<String> lines = readFile(RESULT_TXT);

        assertEquals("CharsCount: 500", lines.get(0));
        assertEquals("WordsCount: 100", lines.get(1));
        assertEquals("CharsWOSpaceCount: 20", lines.get(2));
        assertEquals("CharsPunctuationsCount: 25", lines.get(3));
        assertEquals("Top words:", lines.get(4));
        assertEquals("test", lines.get(5));
    }

    private List<String> readFile(String resultTxt) {
        List<String> lines = new ArrayList<>();
        try (FileReader fr = new FileReader(resultTxt);
             BufferedReader br = new BufferedReader(fr)) {
            String line;
            while ((line = br.readLine()) != null) {
                lines.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return lines;
    }
}
