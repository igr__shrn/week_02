package ru.edu;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class AnalyzerTest {

    /**
     * По умолчанию задачи запускаются с рабочей директорией в корне проекта
     */
    public static final String FILE_PATH = "./src/test/resources/input_text.txt";
    public static final String FILE_PATH_RESULT = "./result.txt";
    public static final int EXPECTED_WORDS = 1719;
    public static final int EXPECTED_CHARS = 8979;
    public static final int EXPECTED_CHARS_WO_SPACES = 7312;
    public static final int EXPECTED_CHARS_ONLY_PUNCT = 339;

    private final TextAnalyzer analyzer = new FileTextAnalyzer(10);
    private final SourceReader  reader = new FileSourceReader();
    private final StatisticReporter reporter = new FileStatisticReporter(FILE_PATH_RESULT);


    @Test
    public void validation() {
        reader.setup(FILE_PATH);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);

        assertEquals(EXPECTED_WORDS, statistics.getWordsCount());
        assertEquals(EXPECTED_CHARS, statistics.getCharsCount());
        assertEquals(EXPECTED_CHARS_WO_SPACES, statistics.getCharsCountWithoutSpaces());
        assertEquals(EXPECTED_CHARS_ONLY_PUNCT, statistics.getCharsCountOnlyPunctuations());

        List<String> topWords = statistics.getTopWords();

        reporter.report(statistics);

        assertEquals(10, topWords.size());

    }
}