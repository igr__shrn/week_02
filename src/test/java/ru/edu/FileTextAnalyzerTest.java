package ru.edu;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FileTextAnalyzerTest {

    @Test
    public void analyzeLine(){
        TextAnalyzer analyzer = new FileTextAnalyzer(10);

        analyzer.analyze("qwerty");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(1, statistics.getWordsCount());
        assertEquals(6, statistics.getCharsCount());
        assertEquals(6, statistics.getCharsCountWithoutSpaces());
        assertEquals(0, statistics.getCharsCountOnlyPunctuations());

        assertEquals(1, statistics.getTopWords().size());
    }

    @Test
    public void analyzeMultiline(){
        TextAnalyzer analyzer = new FileTextAnalyzer(10);

        analyzer.analyze("qwerty");
        analyzer.analyze("\nasdfg, zxcvb");

        TextStatistics statistics = analyzer.getStatistic();

        assertEquals(3, statistics.getWordsCount());
        assertEquals(19, statistics.getCharsCount());
        assertEquals(18, statistics.getCharsCountWithoutSpaces());
        assertEquals(1, statistics.getCharsCountOnlyPunctuations());

        assertEquals(3, statistics.getTopWords().size());
    }
}
