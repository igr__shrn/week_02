package ru.edu;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class FileSourceReaderTest {

    private final static String EXIST_FILE = "./src/test/resources/input_text.txt";
    private final static String TEST_FILE = "./src/test/resources/testSourceReader.txt";
    private final static String NOT_EXIST_FILE = "./not_exist.txt";
    private SourceReader reader = new FileSourceReader();

    @Test
    public void setupExistFile() {
        reader.setup(EXIST_FILE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setupNull() {
        reader.setup(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void notExistFile() {
        reader.setup(NOT_EXIST_FILE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void analyzerNull() {
        reader.readSource(null);
    }
    
    @Test
    public void readSource() {
        TestTextAnalyzer analyzer = new TestTextAnalyzer();

        reader.setup(TEST_FILE);
        TextStatistics statistics = reader.readSource(analyzer);

        assertNotNull(statistics);
        assertEquals(1, statistics.getWordsCount());

        assertEquals("first line", analyzer.lines.get(0));
        assertEquals("\nsecond line", analyzer.lines.get(1));
    }

    static class TestTextAnalyzer implements TextAnalyzer{

        public List<String> lines = new ArrayList<>();

        /**
         * Анализ строки произведения.
         *
         * @param line
         */
        @Override
        public void analyze(String line) {
            lines.add(line);
        }

        /**
         * Получение сохраненной статистики.
         *
         * @return TextStatistic
         */
        @Override
        public TextStatistics getStatistic() {
           TextStatistics statistics = new TextStatistics(10);
           statistics.addWordsCount(1);
           return statistics;
        }
    }
}
