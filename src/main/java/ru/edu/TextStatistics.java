package ru.edu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Collections;
import java.util.Comparator;

/**
 * Необходимо реализовать методы модификации
 * и доступа к хранимым приватным переменным.
 */
public class TextStatistics {

    /**
     * Всего слов.
     */
    private long wordsCount;

    /**
     * Всего символов.
     */
    private long charsCount;

    /**
     * Всего символов без пробелов.
     */
    private long charsCountWithoutSpaces;

    /**
     * Всего знаков препинания.
     */
    private long charsCountOnlyPunctuations;

    /**
     * Топ 10 слов.
     */
    private List<String> topWords = new ArrayList<>();

    /**
     * K - слово, V - количество упоминаний в тексте.
     */
    private Map<String, Integer> wordsMap = new HashMap<>();

    /**
     * Количество топ слов.
     */
    private int toIndex;

    TextStatistics(final int count) {
        toIndex = count;
    }

    /**
     * Добавление количества слов.
     *
     * @param value
     */
    public void addWordsCount(final long value) {
        wordsCount += value;
    }

    /**
     * Добавление количества символов.
     *
     * @param value
     */
    public void addCharsCount(final long value) {
        charsCount += value;
    }

    /**
     * Добавление количества символов, не являющихся пробелами.
     *
     * @param value
     */
    public void addCharsCountWithoutSpaces(final long value) {
        charsCountWithoutSpaces += value;
    }

    /**
     * Добавление количества символов пунктуации.
     *
     * @param value
     */
    public void addCharsCountOnlyPunctuations(final long value) {
        charsCountOnlyPunctuations += value;
    }

    /**
     * Добавление слова в хеш-таблицу и увеличение счетчика упоминаний.
     *
     * @param word
     */
    public void addWordsMap(final String word) {
        if (wordsMap.containsKey(word)) {
            wordsMap.put(word, wordsMap.get(word) + 1);
        } else {
            wordsMap.put(word, 1);
        }
        topWords();
    }

    /**
     * Формирование списка наиболее популярных слов.
     */
    private void topWords() {
        topWords.clear();

        List<Map.Entry<String, Integer>> list = new ArrayList<>(
                wordsMap.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(final Map.Entry<String, Integer> o1,
                               final Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        int toIndexTmp = toIndex;
        if (list.size() < toIndexTmp) {
            toIndexTmp = list.size();
        }
        for (Map.Entry<String, Integer> w : list.subList(0, toIndexTmp)) {
            topWords.add(w.getKey());
        }

    }

    /**
     * Получение количества слов.
     *
     * @return значение
     */
    public long getWordsCount() {
        return wordsCount;
    }

    /**
     * Получение количества символов.
     *
     * @return значение
     */
    public long getCharsCount() {
        return charsCount;
    }

    /**
     * Получение количества слов без пробелов.
     *
     * @return значение
     */
    public long getCharsCountWithoutSpaces() {
        return charsCountWithoutSpaces;
    }

    /**
     * Получение количества знаков препинания.
     *
     * @return значение
     */
    public long getCharsCountOnlyPunctuations() {
        return charsCountOnlyPunctuations;
    }

    /**
     * Задание со звездочкой.
     * Необходимо реализовать нахождение топ-10 слов.
     *
     * @return List из 10 популярных слов
     */
    public List<String> getTopWords() {
        return topWords;
    }

    /**
     * Текстовое представление.
     *
     * @return текст
     */

    @Override
    public String toString() {
        return "TextStatistics{"
                + "wordsCount=" + wordsCount
                + ", charsCount=" + charsCount
                + ", charsCountWithoutSpaces=" + charsCountWithoutSpaces
                + ", charsCountOnlyPunctuations=" + charsCountOnlyPunctuations
                + ", topWords=" + topWords
                + '}';
    }
}
