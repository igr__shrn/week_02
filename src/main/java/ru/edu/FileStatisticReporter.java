package ru.edu;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileStatisticReporter implements StatisticReporter {
    /**
     *
     */
    private File file;

    /**
     * @param filePath - файл для статистики.
     */
    public FileStatisticReporter(final String filePath) {
        file = new File(filePath);
    }

    /**
     * Метод записывает статистику в файл.
     *
     * @param statistics - данные статистики
     */
    @Override
    public void report(final TextStatistics statistics) {
        try (FileOutputStream os = new FileOutputStream(file);
             OutputStreamWriter sw = new OutputStreamWriter(os,
                     StandardCharsets.UTF_8);
             PrintWriter writer = new PrintWriter(sw)) {

            writer.println("CharsCount: " + statistics.getCharsCount());
            writer.println("WordsCount: " + statistics.getWordsCount());
            writer.println("CharsWOSpaceCount: "
                    + statistics.getCharsCountWithoutSpaces());
            writer.println("CharsPunctuationsCount: "
                    + statistics.getCharsCountOnlyPunctuations());
            writer.println("Top words:");

            for (String s : statistics.getTopWords()) {
                writer.println(s);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
