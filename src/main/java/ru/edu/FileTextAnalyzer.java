package ru.edu;

public class FileTextAnalyzer implements TextAnalyzer {

    /**
     * Экземпляр статистики.
     */
    private TextStatistics statistics;

    FileTextAnalyzer(final int count) {
        statistics = new TextStatistics(count);
    }

    /**
     * Анализ строки.
     *
     * @param line
     */
    @Override
    public void analyze(final String line) {
        analyzeWord(line);
        analyzePunctuations(line);
    }

    /**
     * Собирает статистику по словам.
     *
     * @param line
     */
    private void analyzeWord(final String line) {
        String[] words = line.split("[\r \\p{P} \n]");
        for (String word : words) {
            if (!word.equals("")) {
                word = word.toLowerCase();
                statistics.addWordsMap(word);
                statistics.addWordsCount(1);
            }
        }
    }

    /**
     * Собирает статистику по знакам препинания и символам, кроме пробелов.
     *
     * @param line
     */
    private void analyzePunctuations(final String line) {
        char[] symbols = line.toCharArray();
        for (char ch : symbols) {
            statistics.addCharsCount(1);
            if (String.valueOf(ch).matches("\\p{P}")) {
                statistics.addCharsCountOnlyPunctuations(1);
            }
            if (ch != ' ') {
                statistics.addCharsCountWithoutSpaces(1);
            }
        }
    }

    /**
     * Получение сохраненной статистики.
     *
     * @return TextStatistics
     */
    @Override
    public TextStatistics getStatistic() {
        return statistics;
    }
}
