package ru.edu;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.FileReader;

public class FileSourceReader implements SourceReader {

    /**
     *  Файл с текстом.
     */
    private File file;

    /**
     * Установка источника.
     * В реализации тут будет приходить путь до файла-источника
     *
     * @param source - путь до файла, по которому собирается статистика
     */
    @Override
    public void setup(final String source) {
        if (source == null) {
            throw new IllegalArgumentException();
        }
        file = new File(source);
        if (!file.exists()) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * @param analyzer - логика подсчета статистики
     * @return - рассчитанная статистика
     */
    @Override
    public TextStatistics readSource(final TextAnalyzer analyzer) {
        if (analyzer == null) {
            throw new IllegalArgumentException();
        }

        try (FileReader fr = new FileReader(file);
             BufferedReader br = new BufferedReader(fr)) {
            analyzeFile(analyzer, br);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return analyzer.getStatistic();
    }

    private void analyzeFile(final TextAnalyzer analyzer,
                             final BufferedReader br) throws IOException {
        String line;
        boolean firstLine = true;
        while ((line = br.readLine()) != null) {
            analyzer.analyze(!firstLine ? "\n" + line : line);
            firstLine = false;
        }
        br.readLine();
    }
}
